# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta, Pool

__all__ = ['Move']

__metaclass__ = PoolMeta


class Move:
    __name__ = 'stock.move'

    @classmethod
    def distribute(cls, moves, order='ascendant'):
        """ Distributes non-spacial products according their kit configuration.
            If product belongs to a kit the movement will be distribute proportionally to
            other product movements of the kit distributed early

            Returns new movements
        """
        pool = Pool()
        KitLine = pool.get('product.kit.line')
        Uom = pool.get('product.uom')
        Location = pool.get('stock.location')
        Move_model = pool.get('stock.move')

        result = super(Move, cls).distribute(moves, order)

        kits = {}
        # dictionary with kit product.id as key an list of location-quantity tuple as value
        for m in result:
            if not m.product.occupy_space:
                continue
            kit_id = m.product.id
            if not m.product.kit:
                kit_line = KitLine.search([('product', '=', m.product.id)],
                                          limit=1)
                if not kit_line:
                    continue
                kit_line, = kit_line
                kit_id = kit_line.parent.id
            kits.setdefault(kit_id, [])
            qty = m.quantity
            if kit_line:
                qty /= Uom.compute_qty(kit_line.unit, kit_line.quantity, kit_line.product.default_uom, False)
            kits[kit_id].append((m.to_location.id, Uom.compute_qty(m.uom, qty, m.product.default_uom)))

        new_moves = []
        discard_moves = []
        # distributes non spacial products according to kit
        for m in result:
            if m.product.occupy_space:
                continue
            if m.product.kit:
                continue
            assigned_qty = 0
            for key, value in kits.iteritems():
                kit_line = KitLine.search([('parent', '=', key),
                                          ('product', '=', m.product.id)],
                                          limit=1)
                if not kit_line:
                    continue
                kit_line = kit_line[0]
                for location_id, quantity in value:
                    qty = quantity * kit_line.quantity
                    qty = Uom.compute_qty(kit_line.unit, qty, m.uom)
                    new_moves.append(Move_model(planned_date=m.planned_date,
                                                effective_date=getattr(m, 'effective_date', None)
                                                if getattr(m, 'effective_date', None) else None,
                                                product=m.product,
                                                uom=m.uom,
                                                quantity=qty,
                                                from_location=m.from_location,
                                                to_location=Location(location_id),
                                                lot=m.lot,
                                                company=m.company,
                                                currency=m.company.currency if m.company else None,
                                                state=m.state))
                    assigned_qty += qty
                # adjusts quantity
                if m.quantity != assigned_qty:
                    new_moves[len(new_moves) - 1].quantity += (m.quantity - assigned_qty)
                continue

            discard_moves.append(m)

        changes = [m for m in result if m not in discard_moves]
        changes.extend(new_moves)
        return changes