from trytond.pool import Pool
from .stock import *


def register():
    Pool.register(
        Move,
        module='stock_distribute_kit', type_='model')